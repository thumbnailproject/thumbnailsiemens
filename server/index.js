var express = require("express");
var cors = require('cors');
var fs = require("fs");
var app = express();
const PORT = process.env.PORT || 3003;

app.use(cors())

app.use((req, res, next) => {
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.get("/api/data", (req, res) => {
    fs.readFile('./data/templates.json',"utf8", (err, data)=> {
        if(err) res.status(500).send("Error trying to fetch data");
        res.status(200).send(data)
    })
});

app.get("/api/largeData", (req, res) => {
    fs.readFile('./data/extendedTemplate.json', (err, data)=> {
        if(err) res.status(500).send("Error trying to fetch data");;
        res.status(200).send(data)
    }) 
});

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});