How to compile this project:

Backend Server
1.- Go to /server
2.- Run "npm install" in your terminal window.
3.- Run "node index.js" in your terminal window.
4.- you should see "running on port {PORT}"

User Interface:
1.- Go to /templateViewerUI
2.- Run "npm install" in your terminal window.
3.- Run "ng serve" in your terminal window.
4.- You should see "Compled successfully".
5.- Go to http://localhost:4200/

Enjoy!
