import { Component } from '@angular/core';
import { TemplateService } from 'src/services/template.service';
import { ICarouselMeta } from 'src/components/carousel-image/ICarouselMeta';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'templateViewerUI';
  //declare variable of type ICarouselMeta
  carouselItems:ICarouselMeta[] = [];
  selectedCarouselImg;

  constructor(private _templateService: TemplateService) { }

  ngOnInit(): void {
    //fetch image and data from service
    this._templateService.getTemplates().subscribe(res => {
      this.carouselItems = res;
    })
  }
}
