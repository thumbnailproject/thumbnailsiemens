import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HttpClientModule} from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { AppComponent } from './app.component';
import { CarouselImageComponent } from 'src/components/carousel-image/carousel-image.component';
import { ImageLoaderComponent } from 'src/components/image-loader/image-loader.component';
import { FileUploaderComponent } from '../components/file-uploader/file-uploader.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ModalModule } from 'angular-bootstrap-md'

@NgModule({
  declarations: [
    AppComponent,
    CarouselImageComponent,
    ImageLoaderComponent,
    FileUploaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    ModalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
