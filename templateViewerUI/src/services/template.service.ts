import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ICarouselMeta } from 'src/components/carousel-image/ICarouselMeta';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  constructor(private httpClient: HttpClient) { }

  getTemplates(){
    const requestUrl = `${environment.baseUrl}/api/data`;
    return this.httpClient.get<ICarouselMeta[]>(requestUrl);
  }

  getTemplatesLarge(){
    const requestUrl = `${environment.baseUrl}/api/largeData`;
    return this.httpClient.get<ICarouselMeta[]>(requestUrl);
  }
}
