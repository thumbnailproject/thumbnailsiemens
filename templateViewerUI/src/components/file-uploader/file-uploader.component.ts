import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss']
})
export class FileUploaderComponent implements OnInit {
  @Output() onAddImage = new EventEmitter<any>();
  imageForm: FormGroup;
  shouldShowMenu: boolean = false;

  constructor(public formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.imageForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      cost: ['', [Validators.required]],
      id: ['', [Validators.required]],
      description: ['', [Validators.required]],
      thumbnail: ['', [Validators.required]],
      image: ['', [Validators.required]]
    })  
  }

  onSubmit(){
    this.onAddImage.emit(this.imageForm.value);
    this.shouldShowMenu = false;
  }

  showMenu(){
    this.shouldShowMenu = !this.shouldShowMenu;
  }

}
