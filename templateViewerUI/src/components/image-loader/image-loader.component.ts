import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { ICarouselMeta } from '../carousel-image/ICarouselMeta';

@Component({
  selector: 'app-image-loader',
  templateUrl: './image-loader.component.html',
  styleUrls: ['./image-loader.component.scss']
})
export class ImageLoaderComponent implements OnInit {
  @Input() imageData:ICarouselMeta;

  constructor() { }

  ngOnInit(): void {
  }

}
