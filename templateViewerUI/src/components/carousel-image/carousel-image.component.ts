import { Component, Input, OnInit, Output, EventEmitter, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { ICarouselMeta } from './ICarouselMeta';

@Component({
  selector: 'app-carousel-image',
  templateUrl: './carousel-image.component.html',
  styleUrls: ['./carousel-image.component.scss']
})
export class CarouselImageComponent implements OnInit {
  //receive data to render in carousel image
  @Input() thumbMetadata: ICarouselMeta[] = [];
  //return data when a thumbnail is selected
  @Output() onImageSelect = new EventEmitter<ICarouselMeta>();
  THUMBNAILNUMBER = 4;
  splitedImageslength:number;
  pageImages:any = [];
  currentPage:number = 0;
  disablePrevious:boolean = false;
  disableNext:boolean = false;
  splitedArr = [];
  currentSelectionId: string;
  imageToLoad:ICarouselMeta;

  constructor(private cd: ChangeDetectorRef) { }

  //detects when new data is coming from input
  ngOnChanges(changes: SimpleChanges) {
    if(changes.thumbMetadata.currentValue.length > 0){
      this.splitImageMeta(changes.thumbMetadata.currentValue, this.THUMBNAILNUMBER);
    }
  }

  ngOnInit(){
  }

  //split images 4 by 4
  splitImageMeta(thumbMeta, size){
    for(let i = 0; i < thumbMeta.length; i += size) {
      this.splitedArr.push(thumbMeta.slice(i, i+size));
    }
    this.splitedImageslength = this.splitedArr.length;
    this.pageImages = this.splitedArr[this.currentPage];
    if(this.currentPage === 0){
      this.disablePrevious = true;
    }
    this.imageToLoad = this.pageImages[0];
    this.onSelectImage(this.pageImages[0])
  }

  //previous button behavior
  onPrevious(){
    this.disableNext = false;
    if(this.currentPage === 1){
      this.disablePrevious = true;
    }
    this.currentPage -= 1;
    this.pageImages = this.splitedArr[this.currentPage];
  }

//next button behavior
  onNext(){
    this.disablePrevious = false;
    if(this.currentPage === this.splitedArr.length - 2){
      this.disableNext = true;
    }
    this.currentPage += 1;
    this.pageImages = this.splitedArr[this.currentPage];
  }

  //event to select an image
  onSelectImage(image){
    this.currentSelectionId = image.id;
    this.imageToLoad = image;
  }

  onAddImage(newImage){
    newImage['upload'] = true;
    this.thumbMetadata.push(newImage)
    this.splitImageMeta(this.thumbMetadata, this.THUMBNAILNUMBER);
  }

}
