export interface ICarouselMeta{
    title: string,
    cost: string,
    id: string,
    description: string,
    thumbnail: string,
    image: string,
    upload: boolean
}